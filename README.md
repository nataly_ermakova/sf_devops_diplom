# SF_DevOps_Diplom

Сборка и деплой приложения по коммиту через helm чарт в k8s кластер.
Сейчас в YC развернут сервер srv01 (на нем gitlab-runner, grafana, loki, alertmanager, prometheus) и k8s кластер из одной воркер ноды, на котором крутится приложение.

В архиве https://gitlab.com/nataly_ermakova/sf_devops_dip_files/-/tree/main 
terraform код создания инфры, ансибл роли (ключ для подключения), на всякий случай и хелм чарт тоже там, в папке мониторинг компоуз файл, который на srv01 живет, там grafana, loki (в него падают логи из k8s кластера и srv01) и сервисы чтобы метрики и логи лить в графану.

